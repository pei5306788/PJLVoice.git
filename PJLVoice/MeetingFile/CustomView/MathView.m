//
//  MathView.m
//  PJLVoice
//
//  Created by pjl on 2023/12/2.
//  Copyright © 2023 PJL. All rights reserved.
//

#import "MathView.h"

@interface MathView(){
    
    MathNumberLabel *_number1,*_fuhao,*_number2,*_dengyuhao,*_result;
    
}
@end

@implementation MathView

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){
        
        _number1 = [[MathNumberLabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        [self addSubview:_number1];
        
        
        [_number1 mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(5);
            make.top.mas_equalTo(5);
           
            
        }];
        
        
        _fuhao = [[MathNumberLabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        [self addSubview:_fuhao];
        
        _number2 = [[MathNumberLabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        [self addSubview:_number2];
        
        _dengyuhao = [[MathNumberLabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        [self addSubview:_dengyuhao];
        
        _result = [[MathNumberLabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
        
        [self addSubview:_result];
        
        [self setLeftView:_number1 wetRightView:_fuhao ];
        [self setLeftView:_fuhao wetRightView:_number2 ];
        
        [self setLeftView:_number2 wetRightView:_dengyuhao ];
        [self setLeftView:_dengyuhao wetRightView:_result ];
        
    }
    return self;
    
}

- (void)setLeftView:(MathNumberLabel *)leftView
        wetRightView:(MathNumberLabel *)rightView{
    
    
    [rightView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(leftView.mas_right).offset(10);
        
        make.centerY.equalTo(leftView.mas_centerY).offset(0);
        
    }];
    
}

- (void)setMathModel:(MathModel *)mathModel
            withKind:(NSInteger)kind
           withWidth:(CGFloat)masWidth{
    
    self.mathModel = mathModel;
    
    CGFloat maxEachWidth = masWidth/5.0;
    
    
    [_number1 setNumberString:mathModel.number_1
                     withKind:0
                    withWidth:maxEachWidth];
    
    
    [_fuhao setNumberString:mathModel.fuhao 
                   withKind:0
                  withWidth:maxEachWidth];
    
    
    [_number2 setNumberString:mathModel.number_2
                     withKind:0
                    withWidth:maxEachWidth];
    
    [_dengyuhao setNumberString:mathModel.fuhao_2 
                       withKind:0
                      withWidth:maxEachWidth];
    
    [_result setNumberString:mathModel.result withKind:0 withWidth:maxEachWidth];
    
    [self mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.equalTo(self.topViewControl.mas_left).offset(0);
        
        make.right.equalTo(_result.mas_right).offset(0);
        
        make.top.equalTo(self.topViewControl.mas_bottom).offset(10);
        
        make.bottom.equalTo(_result.mas_bottom).offset(0);
    
        
    }];
    
    
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
