//
//  MathNumberLabel.h
//  PJLVoice
//
//  Created by pjl on 2023/12/2.
//  Copyright © 2023 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
NS_ASSUME_NONNULL_BEGIN

@interface MathNumberLabel : UIView

- (void)setNumberString:(NSString *)string
               withKind:(NSInteger)kind
              withWidth:(CGFloat)width;

@end

NS_ASSUME_NONNULL_END
