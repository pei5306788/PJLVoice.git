//
//  MathView.h
//  PJLVoice
//
//  Created by pjl on 2023/12/2.
//  Copyright © 2023 PJL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MathNumberLabel.h"
#import "MathModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MathView : UIView

@property (nonatomic) UIView *topViewControl;

@property (nonatomic) MathModel *mathModel;

- (void)setMathModel:(MathModel *)mathModel
            withKind:(NSInteger)kind
           withWidth:(CGFloat)masWidth;


@end

NS_ASSUME_NONNULL_END
