//
//  MathNumberLabel.m
//  PJLVoice
//
//  Created by pjl on 2023/12/2.
//  Copyright © 2023 PJL. All rights reserved.
//

#import "MathNumberLabel.h"
@interface MathNumberLabel(){
    
    UILabel *_middleLabel;
    
}
@end
@implementation MathNumberLabel

- (id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if(self){
        
        
        _middleLabel = [[UILabel alloc]init];
        
        [_middleLabel setFont:[UIFont systemFontOfSize:17.0]];
        
        [self addSubview:_middleLabel];
        
        
        [_middleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(10);
            make.width.mas_equalTo(20);
            make.height.mas_equalTo(20);
            
        }];
        
        [self.layer setBorderColor:[UIColor systemTealColor].CGColor];
        
        
        self.layer.borderWidth = 1;
        
        
    }
    return self;
    
}

- (void)setNumberString:(NSString *)string
               withKind:(NSInteger)kind
              withWidth:(CGFloat)width{
    
    _middleLabel.text = string;
    
    CGSize middleSize = [_middleLabel sizeThatFits:CGSizeMake(width - 10, CGFLOAT_MAX)];
    
    
    CGFloat middleSizeWidth = middleSize.width;
    CGFloat middleSizeHeiht = middleSize.height;
    
    if(middleSizeWidth == 0){
        
        middleSizeWidth = 20;
    }
    
    middleSizeHeiht = 40;
    
    [_middleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(middleSizeWidth);
        make.height.mas_equalTo(middleSizeHeiht);
        
    }];
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(middleSizeWidth + 20);
        
        make.height.mas_equalTo(middleSizeHeiht + 20);
        
    }];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
