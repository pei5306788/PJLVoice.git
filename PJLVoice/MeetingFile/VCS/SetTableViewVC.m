//
//  SetTableViewVC.m
//  PJLVoice
//
//  Created by pjl on 2023/10/14.
//  Copyright © 2023 PJL. All rights reserved.
//

#import "SetTableViewVC.h"
#import "VCController.h"
@interface SetTableViewVC (){
    
    NSMutableArray *_dataList;
    
}

@end

@implementation SetTableViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataList = [[NSMutableArray alloc]init];
    
    
    [_dataList addObjectsFromArray:@[@"设置教科类型",@"关于我们",@"重新输入学号"]];
    
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return _dataList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    
    
    cell.textLabel.text = _dataList[indexPath.row];
    
    
    
    // Configure the cell...
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *titleStr = _dataList[indexPath.row];
    
    if([titleStr isEqualToString:@"设置教学类型"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"设置教学类型"
                                                                                 message:@"" preferredStyle:(UIAlertControllerStyleActionSheet)];
        
        
        UIAlertAction *cancerClip = [UIAlertAction actionWithTitle:@"取消"
                                                             style:(UIAlertActionStyleCancel)
                                                           handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *netAction = [UIAlertAction actionWithTitle:@"人教版本"
                                                            style:(UIAlertActionStyleDefault)
                                                          handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        
        UIAlertAction *localAction = [UIAlertAction actionWithTitle:@"科教版本"
                                                            style:(UIAlertActionStyleDefault)
                                                          handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:cancerClip];
        
        [alertController addAction:netAction];
        
        [alertController addAction:localAction];
        
        [self presentViewController:alertController animated:true completion:^{
            
        }];
        
    }
    
    if([titleStr isEqualToString:@"关于我们"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"关于我们"
                                                                                 message:@"小学打基础比较重要，主要练习口算，多练习" preferredStyle:(UIAlertControllerStyleAlert)];
        
        
        
        UIAlertAction *makeSure = [UIAlertAction actionWithTitle:@"知道了"
                                                             style:(UIAlertActionStyleCancel)
                                                           handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:makeSure];
        
        [self presentViewController:alertController animated:true completion:^{
            
        }];
        
    }
    
    if([titleStr isEqualToString:@"重新输入学号"]){
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"确定重新输入学号" message:@"返回重新输入学号页面" preferredStyle:(UIAlertControllerStyleAlert)];
        
        
        
        UIAlertAction *makerSure = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
            VCController *vcController = [[VCController alloc]init];
            
            [vcController gotoLogin];
            
        }];
        
        UIAlertAction *cacnerClip = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:makerSure];
        
        [alertController addAction:cacnerClip];
        
        [self presentViewController:alertController animated:true completion:^{
            
        }];
        
        
    }
    
    
    
   
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
