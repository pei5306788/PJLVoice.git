//
//  RegionControllerVC.m
//  PJLVoice
//
//  Created by pjl on 2023/9/14.
//  Copyright © 2023 PJL. All rights reserved.
//

#import "RegionControllerVC.h"

#import "PJLRecognitionObject.h"

#import "MathView.h"

#import "MathControl.h"
@interface RegionControllerVC ()<UITextViewDelegate>{
    
    
    
    MathControl *_MathControl;
    
    
}
@property (weak, nonatomic) IBOutlet UIButton *beginRecordButton;
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;



@property (weak, nonatomic) IBOutlet UIButton *endButton;

@property (nonatomic) MathView *MathView;

@end

@implementation RegionControllerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _MathControl = [[MathControl alloc]init];
    
    _MathControl.superVC = self;
    
    
    [_MathControl readString:@"欢迎使用语音数学20以内的交互学习，下面马上开始练习，主要训练孩子口算，点击下面的开始练习开始练习"];
    
    
    
    
    
//    [self checkButton];
    
    
    _MathView = [[MathView alloc]init];
    
    _MathView.topViewControl = self.topTitleLabel;
    
    
    [self.view addSubview:_MathView];
    
    _MathView.hidden = true;
    
    [_MathView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(0);
        make.top.equalTo( self.topTitleLabel.mas_bottom).offset(10);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        
    }];
    
   
 
    // Do any additional setup after loading the view.
}
//创建个随机假发
- (void)createRandomMathModel{
    
    MathModel *mathModel = [[MathModel alloc]init];
    
    [mathModel checkData];
    
    [_MathView setMathModel:mathModel 
                   withKind:0
                  withWidth:self.view.frame.size.width];
    
    _MathControl.mathView = _MathView;
    
    [_MathControl beignChildrenProgress];
    
    
}

- (IBAction)beginRecrdClip:(id)sender {
    
//    _MathControl = [[MathControl alloc]init];
//    
//    _MathControl.superVC = self;
    
    _MathView.hidden = false;
    
    [self createRandomMathModel];
    
}


- (IBAction)endRecordClip:(id)sender {
    
    [_MathView setHidden:true];
   
}

- (void)checkButton{
    
 
    
}

#pragma paramMark textViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
