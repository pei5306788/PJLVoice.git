//
//  VCController.h
//  PJLVoice
//
//  Created by pjl on 2023/9/14.
//  Copyright © 2023 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface VCController : NSObject

//跳转到首页
- (void)gotoMainTabbar;

//跳转到登录页面
- (void)gotoLogin;

@end

NS_ASSUME_NONNULL_END
