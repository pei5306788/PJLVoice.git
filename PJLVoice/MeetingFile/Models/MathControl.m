//
//  MathControl.m
//  PJLVoice
//
//  Created by pjl on 2023/12/2.
//  Copyright © 2023 PJL. All rights reserved.
//

#import "MathControl.h"
#import "PJLReadObject.h"
#import "PJLRecognitionObject.h"
#import "MBProgressHUD.h"

@interface MathControl(){
    
    PJLReadObject *_PJLReadObject;
    
    PJLRecognitionObject *_PJLRecognitionObject;
    
    BOOL _isbeginRecord;
    
    NSInteger _progress_2;
}

@end


@implementation MathControl

- (id)init{
    
    self = [super init];
    if(self){
        
        _isbeginRecord = NO;
        
        _PJLReadObject = [[PJLReadObject alloc]init];
        
        
        _PJLRecognitionObject = [[PJLRecognitionObject alloc]init];
        __weak typeof(self) __weakSelf = self;
        [_PJLRecognitionObject setListenBlock:^(NSString *str, NSInteger status) {
            
            //识别内容
            if(status == 2){
                
              
                
                NSArray *getArry = [str componentsSeparatedByString:__weakSelf.mathView.mathModel.rightResult];
                
                if(getArry.count == 2){
                    
                  
                    if(self->_progress_2 == 1){
                        
                        self->_progress_2 = 2;
                        __weakSelf.mathView.mathModel.result = __weakSelf.mathView.mathModel.rightResult;
                        
                        [__weakSelf.mathView setMathModel:__weakSelf.mathView.mathModel
                                           withKind:0
                                          withWidth:__weakSelf.superVC.view.frame.size.width];
                        
                        [__weakSelf finshAnswer];
                    }
                    
                }
                
            }
         
          
        }];
        
        //语音朗读
        [_PJLReadObject setAVSpeechSynthesizerDelegatewithPJLVoiceStatusBlock:^(NSInteger status, AVSpeechSynthesizer *synthesizer, AVSpeechUtterance *utterance) {
            
            if(status == 4){
                
              
                
                if(self->_progress_2 == 1){
                    //读完题目，开始等待你的回答
                    [__weakSelf beginRecon];
                    
                }else{
                    
                    [__weakSelf showString:@""
                    withKind:2];
                }
                
                NSLog(@"语音朗读完毕");
            }
                    
        } withPJLVoiceReadingBlock:^(AVSpeechSynthesizer *synthesizer, AVSpeechUtterance *utterance, NSRange characterRange) {
                    
        }];
        
    }
    return self;
}

- (void)finshAnswer{
    
    if(_progress_2 == 2){
        
        //结束编辑
        
        [self showString:@"语音识别结束"
                withKind:1];
        
        [self endRec];
        
    }
    
    
   
    
    
}

//开始语音识别
- (void)beginRecon{
    
    [self showString:@"倾听你的回答(例如：等于12)"
    withKind:0];
    
    
    __weak typeof(self) __weakSelf = self;
    if (@available(iOS 10.0, *)) {
        [_PJLRecognitionObject checkPower:^(SFSpeechRecognizerAuthorizationStatus status) {

            [self->_PJLRecognitionObject startRecording];

            self->_isbeginRecord = true;


        }];
    } else {
        
        [__weakSelf showString:@"语音识别失败"
        withKind:2];
        // Fallback on earlier versions
    }
}
//结束语音识别
- (void)endRec{
    
        _isbeginRecord = false;
       
        [_PJLRecognitionObject endRecording];
     
}
    
- (void)readString:(NSString *)getString{
    
    [self showString:@"朗读中."
    withKind:0];
    
    [_PJLReadObject readWithString:getString
                 withUtteranceRate:0.3
               withPitchMultiplier:0.5
                        withVolume:0.5
             withPreUtteranceDelay:3
            withPostUtteranceDelay:1
                      withLanguage:@"zh-CN"];
    
}


- (void)showString:(NSString *)messageString
          withKind:(NSInteger)kind{
    
    MBProgressHUD *hud = [self getHUd];
    
    if(kind == 0){
        
        hud.mode = MBProgressHUDModeIndeterminate;
       
    }
    
    if(kind == 1){
        
        hud.mode = MBProgressHUDModeText;
        
        [hud hideAnimated:true afterDelay:0.0];
        
    }
    
    if(kind == 2){
        
        hud.mode = MBProgressHUDModeText;
        
        [hud hideAnimated:true afterDelay:0.0];
       
    }
    
    hud.detailsLabel.text = messageString;
    
}

- (MBProgressHUD *)getHUd{
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.superVC.view ];
    if(hud == nil){
        
        hud = [MBProgressHUD showHUDAddedTo:self.superVC.view animated:true];
    }
    
    return hud;
    
}

- (void)beignChildrenProgress{
    
    _progress_2 = 1;
    [self readString:[NSString stringWithFormat:@"算数：%@，请告诉我们答案",self.mathView.mathModel.voiceString]];
}

//是否是纯汉字
- (BOOL)isChinese {
    NSString *match = @"(^[\u4e00-\u9fa5]+$)";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
    return [predicate evaluateWithObject:self];
}

@end
