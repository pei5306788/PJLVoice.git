//
//  MathControl.h
//  PJLVoice
//
//  Created by pjl on 2023/12/2.
//  Copyright © 2023 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MathView.h"
NS_ASSUME_NONNULL_BEGIN

@interface MathControl : NSObject

@property (nonatomic) UIViewController *superVC;

@property (nonatomic) MathView *mathView;


- (void)readString:(NSString *)getString;

- (void)beignChildrenProgress;

@end

NS_ASSUME_NONNULL_END
