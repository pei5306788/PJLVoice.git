//
//  VCController.m
//  PJLVoice
//
//  Created by pjl on 2023/9/14.
//  Copyright © 2023 PJL. All rights reserved.
//

#import "VCController.h"
#import "AppDelegate.h"
@interface VCController(){
    
}

@end

@implementation VCController

- (id)init{
    
    self = [super init];
    if(self){
        
    }
    return self;
    
}

- (void)gotoMainTabbar{
    
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    UITabBarController *getTabbarController = [main  instantiateViewControllerWithIdentifier:@"MyTabbarController"];
    
    AppDelegate *appDelegate =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    appDelegate.window.rootViewController = getTabbarController;
     
}


- (void)gotoLogin{
    
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    UIViewController *getVC = [main  instantiateViewControllerWithIdentifier:@"LoginVCID"];
    
    AppDelegate *appDelegate =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    appDelegate.window.rootViewController = getVC;
     
}

@end
