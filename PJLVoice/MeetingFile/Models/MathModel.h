//
//  MathModel.h
//  PJLVoice
//
//  Created by pjl on 2023/12/2.
//  Copyright © 2023 PJL. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MathModel : NSObject

@property (nonatomic) NSString *number_1;

@property (nonatomic) NSString *fuhao;

@property (nonatomic) NSString *number_2;

@property (nonatomic) NSString *fuhao_2;

@property (nonatomic) NSString *voiceString;//语音读法

//小孩的回答
@property (nonatomic) NSString *result;

//正确答案
@property (nonatomic) NSString *rightResult;

//答案算法

@property (nonatomic) NSString *reason;


- (void)checkData;

@end

NS_ASSUME_NONNULL_END
