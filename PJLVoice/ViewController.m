//
//  ViewController.m
//  PJLVoice
//
//  Created by 裴建兰 on 2018/3/29.
//  Copyright © 2018年 PJL. All rights reserved.
//

#import "ViewController.h"
#import "PJLReadObject.h"
#import "PJLRecognitionObject.h"
@interface ViewController (){
    
  
    
}
@property (weak, nonatomic) IBOutlet UILabel *ShowMessageLabels;

@property (nonatomic)  PJLRecognitionObject *record;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor redColor];
    
   _record = [[PJLRecognitionObject alloc]init];
    
//    __weak typeof(self) __weakSelf = self;
//    [_record setListenBlock:^(NSString *str, NSInteger status) {
//        if(str != nil){
//            __weakSelf.ShowMessageLabels.text = str;
//
//        }
//    }];
    
    NSString *getLocal = [[NSBundle mainBundle]pathForResource:@"18514798-61a5-499e-a0a6-1b6f34c9e685" ofType:@"wav"];
    
    getLocal = [NSString stringWithFormat:@"file://%@",getLocal];

    if (@available(iOS 10.0, *)) {
         __weak typeof(self) __weakSelf = self;
        [_record recognizeLocalAudioFile:getLocal
                         withRecordBlock:^(NSString *recordStr) {
                        if(recordStr != nil){
                            __weakSelf.ShowMessageLabels.text = recordStr;
                                 
                        }
                             
                         } whithLocaleIdentifier:nil];
        
    } else {
        // Fallback on earlier versions
    }
    

    
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)begin:(id)sender {

    __weak typeof(self) __weakSelf = self;
    if (@available(iOS 10.0, *)) {
        [_record checkPower:^(SFSpeechRecognizerAuthorizationStatus status) {
            
            [__weakSelf.record startRecording];
            
            
        }];
    } else {
        // Fallback on earlier versions
    }
}

- (IBAction)end:(id)sender {

    [_record endRecording];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
//    PJLReadObject *test = [[PJLReadObject alloc]init];
    
//    [test setReadTest:@"水调歌头·明月几时有"
//     "[宋] 苏轼"
//     "丙辰中秋，欢饮达旦，大醉，作此篇。兼怀子由。"
//     "明月几时有，把酒问青天。"
//     "不知天上宫阙，今夕是何年？"
//     "我欲乘风归去，又恐琼楼玉宇，"
//     "高处不胜寒。"
//     "起舞弄清影，何似在人间！"
//     "转朱阁，低绮户，照无眠。"
//     "不应有恨，何事长向别时圆？"
//     "人有悲欢离合，月有阴晴圆缺，"
//     "此事古难全。"
//     "但愿人长久，千里共婵娟"];
//    [test setReadTest:@"Hellow! what is your Name,你好 请问你的名字是什么啊！"];
    
   
   
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
